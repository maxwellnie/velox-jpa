package com.maxwellnie.velox.jpa.framework.java;

/**
 * @author Maxwell Nie
 */
public interface Delegate {
    Object doExecute(Object[] args);
}
