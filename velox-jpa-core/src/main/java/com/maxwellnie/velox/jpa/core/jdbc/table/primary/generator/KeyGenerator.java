package com.maxwellnie.velox.jpa.core.jdbc.table.primary.generator;

public interface KeyGenerator {

    Object nextKey();
}
