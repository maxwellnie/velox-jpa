package com.maxwellnie.velox.jpa.core.exception;

/**
 * @author Maxwell Nie
 */
public class NotMappedMethodException extends RuntimeException {
    public NotMappedMethodException(String message) {
        super(message);
    }
}
