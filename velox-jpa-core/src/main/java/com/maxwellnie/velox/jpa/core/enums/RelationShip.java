package com.maxwellnie.velox.jpa.core.enums;

/**
 * @author Maxwell Nie
 */
public enum RelationShip {
    AND,
    OR
}
