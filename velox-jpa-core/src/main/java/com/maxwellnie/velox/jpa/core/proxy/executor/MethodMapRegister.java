package com.maxwellnie.velox.jpa.core.proxy.executor;

/**
 * @author Maxwell Nie
 */
public interface MethodMapRegister {
    void registerDaoImpl(Class<?> clazz);
}
