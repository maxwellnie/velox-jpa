package com.maxwellnie.velox.jpa.core.dao.support.env;

/**
 * @author Maxwell Nie
 */
public interface EnvironmentUser {
    Environment getEnvironment();
}
