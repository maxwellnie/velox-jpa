package com.maxwellnie.velox.jpa.core.exception;

/**
 * @author Maxwell Nie
 */
public class PrimaryKeyException extends RuntimeException {
    public PrimaryKeyException(String message) {
        super(message);
    }
}
