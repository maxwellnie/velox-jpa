package com.maxwellnie.velox.jpa.core.exception;

/**
 * @author Maxwell Nie
 */
public class NotIntegratingResourceException extends RuntimeException {
    public NotIntegratingResourceException(String message) {
        super(message);
    }
}
