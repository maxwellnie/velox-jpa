package com.maxwellnie.velox.jpa.core.exception;

/**
 * @author Maxwell Nie
 */
public class ThreadPoolException extends RuntimeException {
    public ThreadPoolException(String message) {
        super(message);
    }
}
