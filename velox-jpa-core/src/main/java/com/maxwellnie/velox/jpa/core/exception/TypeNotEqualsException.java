package com.maxwellnie.velox.jpa.core.exception;

public class TypeNotEqualsException extends RuntimeException {
    public TypeNotEqualsException() {
    }

    public TypeNotEqualsException(String message) {
        super(message);
    }
}
