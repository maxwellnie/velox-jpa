/**
 * 框架中很多的组件类只是提供了方法，并没有储存其他数据，这个机制的设计只是为了减少不必要的对象建造情况。
 *
 * @author Maxwell Nie
 */
package com.maxwellnie.velox.jpa.core.manager;



